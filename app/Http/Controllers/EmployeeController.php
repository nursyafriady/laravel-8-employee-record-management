<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Department, Employee};

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Employee::with('Department')->orderByDesc('id')->get();
        // dd($data);
        return view('pages.admin.employee.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Department::all();
        return view('pages.admin.employee.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'address' => 'required',
            'department' => 'required',
            'photo' => 'required|image|mimes:jpg,jpeg,png,svg,gif',
            'mobile_number' => 'required',
            'status' => 'required'
        ]);

        $photo = $request->file('photo');
        $renamePhoto = time().'.'.$photo->getClientOriginalExtension();
        $dest = public_path('/images');
        $photo->move($dest, $renamePhoto);

        $data = new Employee;
        $data->full_name = $request->full_name;
        $data->address = $request->address;
        $data->photo = $renamePhoto;
        $data->department_id = $request->department;
        $data->mobile_number = $request->mobile_number;
        $data->status = $request->status;
        $data->save();

        return redirect()->route('employee.create')->with('success', 'data has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Employee::find($id);
        return view('pages.admin.employee.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::find($id);
        $depart = Department::all();
        return view('pages.admin.employee.edit', compact('data', 'depart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'full_name' => 'required',
            'address' => 'required',
            'department' => 'required',
            'photo' => 'image|mimes:jpg,jpeg,png,svg,gif',
            'mobile_number' => 'required',
            'status' => 'required'
        ]);

        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $renamePhoto = time().'.'.$photo->getClientOriginalExtension();
            $dest = public_path('/images');
            $photo->move($dest, $renamePhoto);

            $image_path = 'images/'.$request->prev_photo; 
            // dd($image_path);
            if(\File::exists($image_path)){
                \File::delete($image_path);
            }
        } else {
            $renamePhoto = $request->prev_photo;
        }

    
        $data = Employee::find($id);
        $data->full_name = $request->full_name;
        $data->address = $request->address;
        $data->photo = $renamePhoto;
        $data->department_id = $request->department;
        $data->mobile_number = $request->mobile_number;
        $data->status = $request->status;
        $data->save();

        return redirect()->route('employee.edit', $id)->with('success', 'data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Employee::where('id', $id)->delete();
        $data = Employee::find($id);
        $data->delete();
        $image_path = 'images/'.$data->photo; 
        // dd($image_path);
        if(\File::exists($image_path)){
            \File::delete($image_path);
        }
        return redirect()->route('employee.index')->with('success', 'data has been deleted');
    }
}
