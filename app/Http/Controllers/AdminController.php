<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
        $data = Employee::orderByDesc('id', 'created_at')->get()->groupBy(function($data) {
            return Carbon::parse($data->created_at)->format('M');
        });

        $months = [];
        $monthCount = [];
        foreach($data as $month => $values) {
            $months[] = $month;
            $monthCount[] = count($values);
        }

        return view('pages.admin.dashboard', compact('data', 'months', 'monthCount'));
    }
}
