@extends('layouts.admin')
@section('title', 'Create department')

@section('content')
    <div class="card mb-4 mt-3 mx-3">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
                Create Department
            <a href="{{ route('department.index') }}" class="float-end btn btn-primary btn-sm">View All</a>
        </div>
        <div class="card-body">
            @if(Session::has('success'))
                <p class="text-success">{{ session('success') }}</p>
            @endif

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            @endif

            <form action="{{ route('department.store') }}" method="post">
                @csrf
                <table class="table table-bordered">
                    <tr>
                        <th>Title</th>
                        <td>
                            <input type="text" name="title" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="submit" class="btn btn-success">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
@endsection