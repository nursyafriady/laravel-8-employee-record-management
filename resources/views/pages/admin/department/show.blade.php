@extends('layouts.admin')
@section('title', 'Show Department')

@section('content')
    <div class="card mb-4 mt-3 mx-3">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
                Show Department : <span class="fw-bold">{{ $data->title }}</span> 
            <a href="{{ route('department.index') }}" class="float-end btn btn-primary btn-sm">View All</a>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Title</th>
                    <td>
                        {{ $data->title }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection