@extends('layouts.admin')
@section('title', 'All department')

@section('content')
<div class="card mb-4 mt-3 mx-3">
    <div class="card-header">
        <i class="fas fa-table me-1"></i>
        All Department
        <a href="{{ route('department.create') }}" class="float-end btn btn-primary btn-sm">Add New</a>
    </div>
    <div class="card-body">
        <table id="datatablesSimple">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <tbody>
                @if($data)
                    @foreach($data as $key=>$d)
                        <tr>
                            <td style="width: 5%;">{{ $key+1 }}</td>
                            <td style="width: 75%;">{{ $d->title }}</td>
                            <td style="width: 20%;">
                                <a href="{{ route('department.show', $d->id) }}" class="btn btn-sm btn-warning">Show</a>
                                <a href="{{ route('department.edit', $d->id) }}" class="btn btn-sm btn-primary">Update</a>
                                <a 
                                    href="{{ route('department-delete', $d->id) }}" 
                                    class="btn btn-sm btn-danger"
                                    onclick="return confirm('are you sure to delete?')">
                                        Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection