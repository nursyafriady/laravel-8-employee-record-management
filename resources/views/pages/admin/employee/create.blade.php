@extends('layouts.admin')
@section('title', 'Create Employee')

@section('content')
    <div class="card mb-4 mt-3 mx-3">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
                Create Employee
            <a href="{{ route('employee.index') }}" class="float-end btn btn-primary btn-sm">View All</a>
        </div>
        <div class="card-body">
            @if(Session::has('success'))
                <p class="text-success">{{ session('success') }}</p>
            @endif

            @if($errors->any())
                @foreach($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                @endforeach
            @endif

            <form action="{{ route('employee.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <table class="table table-bordered">
                    <tr>
                        <th>Full Name</th>
                        <td>
                            <input type="text" name="full_name" class="form-control" value="{{ old('full_name') }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Department</th>
                        <td>
                            <select name="department" id="department" class="form-control">
                                <option value="">-- Select Departement ---</option>
                                @foreach($data as $d)
                                    <option value="{{ $d->id }}">{{ $d->title }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>photo</th>
                        <td>
                            <input type="file" name="photo" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>
                            <input type="text" name="address" class="form-control" value="{{ old('address') }}">
                        </td>
                    </tr>
                    <tr>
                        <th>mobile</th>
                        <td>
                            <input type="text" name="mobile_number" class="form-control" value="{{ old('mobile_number') }}">
                        </td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>
                             <input type="radio" name="status" value="1"> Activate <br>
                             <input type="radio" name="status" value="0" checked="checked"> Deactivate
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="submit" class="btn btn-success">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
@endsection