@extends('layouts.admin')
@section('title', 'Show Employee')

@section('content')
    <div class="card mb-4 mt-3 mx-3">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
                Show Employee
            <a href="{{ route('employee.index') }}" class="float-end btn btn-primary btn-sm">View All</a>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>Full Name</th>
                    <td>
                        {{ $data->full_name }}
                    </td>
                </tr>
                <tr>
                    <th>Department</th>
                    <td>
                        {{ $data->department->title }}
                    </td>
                </tr>
                <tr>
                    <th>photo</th>
                    <td>
                        <img src="{{ asset('images/'.$data->photo) }}" alt="" width="200">
                    </td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td>
                        {{ $data->address }}
                    </td>
                </tr>
                <tr>
                    <th>mobile</th>
                    <td>
                        {{ $data->mobile_number }}
                    </td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>
                            @if($data->status == 1) Activated @else Deactivated @endif
                    </td>
                </tr>
            </table>
        </div>
@endsection