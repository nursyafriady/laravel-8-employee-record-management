<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.admin.meta')
        @include('includes.admin.style')
    </head>
    <body class="sb-nav-fixed">
        @include('includes.admin.navbar')
        <div id="layoutSidenav">
            @include('includes.admin.sidebar')
            <div id="layoutSidenav_content">
                <main>
                  @yield('content')
                </main>
            @include('includes.admin.footer')   
            </div>
        </div>
        @include('includes.admin.script') 
    </body>
</html>
