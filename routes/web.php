<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{AdminController, DepartmentController, EmployeeController};

Route::get('/admin', [AdminController::class, 'index'])->name('dashboard');

Route::get('department/{id}/delete', [DepartmentController::class, 'destroy'])->name('department-delete');
Route::resource('department', DepartmentController::class);

Route::get('employee/{id}/delete', [EmployeeController::class, 'destroy'])->name('employee-delete');
Route::resource('employee', EmployeeController::class);



// Route::get('/', function () {
//     return view('welcome');
// });